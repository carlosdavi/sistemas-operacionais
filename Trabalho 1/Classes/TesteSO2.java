/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeso2;

import javax.swing.JTextArea;

/**
 *
 * @author Carlos David
 */
public class TesteSO2{

    private final int hospedes;
    private final int tempoDormir;
    private final int tempoAssistir;
    private final JTextArea log;
    private final int qtdCanais;

    /**
     * @param args the command line arguments
     */
    public TesteSO2(int hospedes, int tempoDormir,int tempoAssistir, JTextArea log, int qtdCanais){
     this.hospedes  = hospedes;
     this.tempoDormir = tempoDormir;
     this.tempoAssistir  = tempoAssistir;
     this.log = log;
     this.qtdCanais = qtdCanais;
     for(int i = 0; i < hospedes; i++){
      new Thread(new Hospede(this.tempoAssistir,this.tempoDormir,this.qtdCanais,log)).start();
     }
    
    }
    
  /*  public static void main(String[] args) {
        new Thread(new Hospede(3,2,5)).start();
        new Thread(new Hospede(3,2,5)).start();
        new Thread(new Hospede(3,2,5)).start();
        new Thread(new Hospede(3,2,5)).start();
        new Thread(new Hospede(3,2,5)).start();
        
        
    }*/
    
}
