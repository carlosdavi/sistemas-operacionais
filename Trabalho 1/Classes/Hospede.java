/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeso2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author Carlos David
 */
public class Hospede extends Thread{

    private static int canalTv;
    private static Semaphore mutex1 =  new Semaphore(1);
    private static Semaphore mutex2 =  new Semaphore(0);
    private static Semaphore livre =  new Semaphore(1);
    private int canalFavorito;
    private int tempoAssistir;
    private int tempoDormir;
    private int hospedeId;
    private final JTextArea log;
    
    public Hospede(int tempoAssistir,int tempoDormir, int qtdCanais, JTextArea log){
        Random rand = new Random();
        this.log = log;
        int id = 0;
        do
           {
            this.tempoAssistir = rand.nextInt(tempoAssistir+1);
            this.tempoDormir = rand.nextInt(tempoDormir+1);
            this.canalFavorito = rand.nextInt(qtdCanais+1);
            this.hospedeId = rand.nextInt(100);
            id++;
            }
        while((tempoAssistir == 0) || (tempoDormir == 0) || (canalFavorito == 0));
        }
    @Override
    public void run(){
    while(true)
        {
            try {                 
                Hospede.mutex1.acquire();
                if(Hospede.livre.availablePermits() == 1){
               
                    Hospede.livre.acquire();
                    
                    Hospede.canalTv = this.canalFavorito;
                    System.err.println("Hospede: "+this.hospedeId+" mudou o canal da TV. Canal:"+Hospede.canalTv);                    
                    log.setText(log.getText()+ "\n Hospede: "+this.hospedeId+" mudou o canal da TV. Canal:"+Hospede.canalTv);
                    Hospede.mutex2.release();  
                    
                    Hospede.mutex1.release();
                    // DESCANSA();    
                    System.err.println("Hospede: "+this.hospedeId+" assistindo.");
                    log.setText(log.getText()+ "\n Hospede: "+this.hospedeId+" assistindo.");

                    ContaTempo(this.tempoAssistir);
                    System.err.println("Hospede: "+this.hospedeId+" descansando.");
                    log.setText(log.getText()+ "\n Hospede: "+this.hospedeId+" descansando.");
                    System.err.println("----------------------");
                    log.setText(log.getText()+ "\n ----------------------");

                    ContaTempo(this.tempoDormir);                    
                    System.err.println("Hospede: "+this.hospedeId+" parou de assistir. TV agora e: "+Hospede.canalTv);
                    log.setText(log.getText()+ "\n Hospede: "+this.hospedeId+" parou de assistir. TV agora e: "+Hospede.canalTv);

                    Descansar();
                }
                else if(Hospede.canalTv == this.canalFavorito){
                    Hospede.mutex2.release();
                    System.out.println("Hospede: "+hospedeId+" tem o mesmo canal."+Hospede.canalTv);
                    log.setText(log.getText()+ "\n Hospede: "+hospedeId+" tem o mesmo canal."+Hospede.canalTv);

                    Hospede.mutex1.release();                    
                    //m1.acquire();
                    System.out.println("Hospede: "+hospedeId+" assistindo.");
                    log.setText(log.getText()+ "\n Hospede: "+hospedeId+" assistindo.");
                    ContaTempo(this.tempoAssistir);
                    System.out.println("Hospede: "+hospedeId+" descansando.");
                    log.setText(log.getText()+ "\n Hospede: "+hospedeId+" descansando.");
                    ContaTempo(this.tempoDormir);
                    Descansar();
                                   
                }
                else{                    
                    System.out.println("Hospede: "+hospedeId+" dormindo.");
                     log.setText(log.getText()+ "\n Hospede: "+hospedeId+" dormindo.");
                    Hospede.mutex1.release();                    
                    Hospede.livre.acquire();  
                    
                }
        
    }   catch (InterruptedException ex) {
        System.out.println("Houve Exception");
            Logger.getLogger(Hospede.class.getName()).log(Level.SEVERE, null, ex);
            
        }
            
    
    }
    
    
}
    
    public void Descansar(){
        try {
            Hospede.mutex2.acquire();
             if(Hospede.mutex2.availablePermits() == 0){
                        System.out.println("Valor de livre:"+livre.availablePermits());
                        log.setText(log.getText()+ "\n Valor de livre:"+livre.availablePermits());
                        Hospede.livre.release();
                        System.out.println("Agora o valor de livre:"+livre.availablePermits());
                        log.setText(log.getText()+ "\n Agora o valor de livre:"+livre.availablePermits());
                        Hospede.canalTv = 0;
                        System.err.println("Hospede: "+this.hospedeId+" parou de assistir. TV agora e: "+Hospede.canalTv);
                        log.setText(log.getText()+ "\n Hospede: "+this.hospedeId+" parou de assistir. TV agora e: "+Hospede.canalTv);
             }
        } catch (InterruptedException ex) {
            Logger.getLogger(Hospede.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
        
        
        
    
public boolean ContaTempo(int quantidadeSegundos){
            Date date = new Date();
            DateFormat formato = new SimpleDateFormat("ss");
            String formattedDate = formato.format(date);    
            Integer tempoFD = (Integer.parseInt(formattedDate));
            Integer tempoFinal = tempoFD+quantidadeSegundos;
            while(tempoFD < tempoFinal){
                formattedDate = formato.format(new Date());
                tempoFD = (Integer.parseInt(formattedDate));
            }
        return true;
    
}
}